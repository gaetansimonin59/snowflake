from random import randrange
from math import pi
from math import sqrt
from PIL import Image, ImageDraw

temps_max = 2501
NOMBRE_DE_LIGNE = 200
cote = 1
demi_hauteur = sqrt(cote**2-(cote//2)**2)
longueur = int(((3/2*NOMBRE_DE_LIGNE)+2)*cote)
largeur = int((NOMBRE_DE_LIGNE*2+1)*demi_hauteur)


BLACK = (0, 0, 0)

def coordonnees(n):
    """
    Renvoie une liste de tuple des coordonnées d'un tableau allant de 0 à n
    : param n : (int) nombre de ligne et de colonne dans le tableau
    : return : (list) liste de tuple des coordonnees de tous le points du tableau

    CU : n >= 0

    Tests :
    >>> coordonnees(0)
    [(0, 0)]
    >>> coordonnees(3)
    [(0, 0), (1, 0), (2, 0), (3, 0), (0, 1), (1, 1), (2, 1), (3, 1), (0, 2), (1, 2), (2, 2), (3, 2), (0, 3), (1, 3), (2, 3), (3, 3)]
    """
    l=[]
    for i in range(n+1) :
        for j in range(n+1) :
            l.append((j,i))
    return l

def voisins(coordonnees):
    """
    Renvoie la liste des tuples de coordonnées des voisins d'un hexagone dans le pavage hexagonal
    : param coordonnnees : (tuple) tuple de coordonnées de l'hexagone dans le pavage hexagonal
    : return : (list) liste de coordonnées des voisins de l'hexagone dans le pavage hexagonal

    CU : pour un tuple de coordonnées (x,y) il faut que x et y soient positifs ou nuls et len(coordonnees) == 2

    Tests :
    >>> voisins((0,0))
    [(1, 0), (0, 1)]
    >>> voisins((1,0))
    [(2, 0), (2, 1), (1, 1), (0, 1), (0, 0)]
    >>> voisins((2,0))
    [(3, 0), (2, 1), (1, 0)]
    >>> voisins((NOMBRE_DE_LIGNE,0))
    [(200, 1), (199, 0)]
    >>> voisins((NOMBRE_DE_LIGNE,20))
    [(200, 19), (200, 21), (199, 20), (199, 19)]
    >>> voisins((20,NOMBRE_DE_LIGNE))
    [(20, 199), (21, 199), (21, 200), (19, 200), (19, 199)]
    >>> voisins((21,NOMBRE_DE_LIGNE))
    [(21, 199), (22, 200), (20, 200)]
    >>> voisins((NOMBRE_DE_LIGNE,NOMBRE_DE_LIGNE))
    [(200, 199), (199, 200), (199, 199)]
    >>> voisins((0,NOMBRE_DE_LIGNE))
    [(0, 199), (1, 199), (1, 200)]
    >>> voisins((0,20))
    [(0, 19), (1, 19), (1, 20), (0, 21)]
    >>> voisins((4,5))
    [(4, 4), (5, 4), (5, 5), (4, 6), (3, 5), (3, 4)]
    >>> voisins((5,4))
    [(5, 3), (6, 4), (6, 5), (5, 5), (4, 5), (4, 4)]
    """
    j=coordonnees[0]
    i=coordonnees[1]
    liste=[]
    if j%2 == 0 :
        v = [(j,i-1),(j+1,i-1),(j+1,i),(j,i+1),(j-1,i),(j-1,i-1)]
    elif j%2 == 1:
        v = [(j,i-1),(j+1,i),(j+1,i+1),(j,i+1),(j-1,i+1),(j-1,i)]
    for t in v :
        if not(t[0]<0 or t[1]<0 or t[0]>NOMBRE_DE_LIGNE or t[1]>NOMBRE_DE_LIGNE) :
            liste.append(t)
    return liste


def coordonnees_dictionnaire(list_coord,p):
    """
    Renvoie un dictionnaire de coordonnées avec pour chaque tuple de coordonnées une valeur de b, c, d et la liste des voisins associées à cette coordonnées
    : param list_coord : (list) liste de tuples de coordonnées de tout les hexagones/cellules du pavage hexagonal
    : param p : (float) proportion initiale de vapeur dans chaque cellule hormis le centre
    : return : (dict) renvoie un dictionnaire de coordonnées avec pour chaque tuple de coordonnées une valeur de b, c, d et la liste des voisins associées à cette coordonnée

    CU : 0 < p

    Tests :
    >>> coordonnees_dictionnaire([],0.3)
    {}
    >>> coordonnees_dictionnaire([(0, 0), (1, 0), (2, 0), (3, 0), (0, 1), (1, 1), (2, 1), (3, 1), (0, 2), (1, 2), (2, 2), (3, 2), (0, 3), (1, 3), (2, 3), (3, 3)],0.3)
    {(0, 0): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(1, 0), (0, 1)]}, (1, 0): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(2, 0), (2, 1), (1, 1), (0, 1), (0, 0)]}, (2, 0): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(3, 0), (2, 1), (1, 0)]}, (3, 0): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(4, 0), (4, 1), (3, 1), (2, 1), (2, 0)]}, (0, 1): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(0, 0), (1, 0), (1, 1), (0, 2)]}, (1, 1): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(1, 0), (2, 1), (2, 2), (1, 2), (0, 2), (0, 1)]}, (2, 1): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(2, 0), (3, 0), (3, 1), (2, 2), (1, 1), (1, 0)]}, (3, 1): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(3, 0), (4, 1), (4, 2), (3, 2), (2, 2), (2, 1)]}, (0, 2): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(0, 1), (1, 1), (1, 2), (0, 3)]}, (1, 2): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(1, 1), (2, 2), (2, 3), (1, 3), (0, 3), (0, 2)]}, (2, 2): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(2, 1), (3, 1), (3, 2), (2, 3), (1, 2), (1, 1)]}, (3, 2): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(3, 1), (4, 2), (4, 3), (3, 3), (2, 3), (2, 2)]}, (0, 3): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(0, 2), (1, 2), (1, 3), (0, 4)]}, (1, 3): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(1, 2), (2, 3), (2, 4), (1, 4), (0, 4), (0, 3)]}, (2, 3): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(2, 2), (3, 2), (3, 3), (2, 4), (1, 3), (1, 2)]}, (3, 3): {'b': 0, 'c': 0, 'd': 0.3, 'voisins': [(3, 2), (4, 3), (4, 4), (3, 4), (2, 4), (2, 3)]}}
    """
    dico = dict()
    for t in list_coord :
        dico[t] = dict()
        if t == (NOMBRE_DE_LIGNE//2,NOMBRE_DE_LIGNE//2) :
            dico[t]['b'] = 0
            dico[t]['c'] = 1
            dico[t]['d'] = 0
            dico[t]['voisins'] = voisins(t)
            dico[t]['appartenance'] = True
            dico[t]['couleur'] = (0,0,255)
        else :
            dico[t]['b'] = 0
            dico[t]['c'] = 0
            dico[t]['d'] = p
            dico[t]['voisins'] = voisins(t)
            dico[t]['appartenance'] = False
            dico[t]['couleur'] = (0,0,0)
    return dico

dictionnaire = coordonnees_dictionnaire(coordonnees(NOMBRE_DE_LIGNE),0.1)

def cristal(liste) :
    """
    Renvoie l'ensemble des tuples de coordonnées des hexagones présents dans le cristal/flocon
    : param liste : (list) liste de tuples de coordonnées de tout les hexagones du pavage hexagonal
    : return : (set) renvoie l'ensemble des coordonnées des hexagones présents dans le cristal/flocon

    CU : aucune

    Tests :
    >>> cristal([])
    set()
    >>> cristal(coordonnees(NOMBRE_DE_LIGNE))
    {(100, 100)}
    """
    cristal = set()
    for i in liste :
        if dictionnaire[i]['appartenance'] == True :
            cristal.add(i)
    return cristal

CRISTAL = cristal(coordonnees(NOMBRE_DE_LIGNE))

def frontiere(liste) :
    """
    Renvoie l'ensemble des tuples de coordonnées des hexagones de la frontière du flocon/cristal
    : param liste : (list) liste de tuples de coordonnées de tout les hexagones du pavage hexagonal
    : return : (set) Renvoie l'ensemble des tuples de coordonnées des hexagones de la frontière du flocon/cristal

    CU : aucune

    Tests:
    >>> frontiere([])
    set()
    >>> frontiere(coordonnees(NOMBRE_DE_LIGNE))
    {(101, 100), (99, 99), (100, 101), (100, 99), (99, 100), (101, 99)}
    """
    frontiere = set()
    for coord in liste :
        for i in dictionnaire[coord]['voisins'] :
            if dictionnaire[i]['appartenance'] == True and not(coord in CRISTAL) :
                frontiere.add(coord)
    return frontiere

FRONTIERE = frontiere(coordonnees(NOMBRE_DE_LIGNE))

def diffusion(coord):
    """
    Définie le fonctionnement de la diffusion de l'état de vapeur dans le milieu autour du flocon
    : param coord : (tuple) tuple de coordonnées de l'hexagone dans le pavage hexagonal
    : return : None

    CU : pour un tuple de coordonnées (x,y) il faut que x et y soient positifs ou nuls et len(coord) == 2
    """
    voisin = dictionnaire[coord]['voisins']
    somme = dictionnaire[coord]['d']

    if dictionnaire[coord]['appartenance'] == False and not(coord in FRONTIERE) :
        for t in voisin :
            somme += dictionnaire[t]['d']

    elif coord in FRONTIERE :
        for t in voisin :
            if dictionnaire[t]['appartenance'] == True :
                somme += dictionnaire[coord]['d']
            else:
                somme += dictionnaire[t]['d']

    if dictionnaire[coord]['appartenance'] == False :
        d = somme / (len(voisin) + 1)
        dictionnaire[coord]['d'] = d
    return

def gel(k):
    """
    Définie le gel dans toutes les cellules de la frontiere
    : param k : (float) proportion de vapeur qui se transforme en glace pour un hexagone de la frontière
    : return : None

    CU : 0 <= k
    """
    for coord in FRONTIERE :
        dictionnaire[coord]['b'] = dictionnaire[coord]['b'] + (1-k) * dictionnaire[coord]['d']
        dictionnaire[coord]['c'] = dictionnaire[coord]['c'] + k * dictionnaire[coord]['d']
        dictionnaire[coord]['d'] = 0
    return

def fonte(u,y):
    """
    Définie la fonte dans tout les hexagones de la frontière
    : param u : (float) coefficient de la phase de fonte
    : param y : (float) coefficient de la phase de fonte
    : return : None

    CU : 0 <= u and 0 <= y
    """
    for coord in FRONTIERE :
        dictionnaire[coord]['d'] = dictionnaire[coord]['d'] + u * dictionnaire[coord]['b'] + y * dictionnaire[coord]['c']
        dictionnaire[coord]['b'] = (1 - u) * dictionnaire[coord]['b']
        dictionnaire[coord]['c'] = (1 - y) * dictionnaire[coord]['c']
    return

def attachement(a,B,O,tt):
    """
    Définie l'attachement ou non des hexagones de la frontiere du flocon
    : param a : (float) coefficient intervenant lors de la phase d'attachement, détermine si l'hexagone fait partie ou non du flocon
    : param B : (float) coefficient intervenant lors de la phase d'attachement, détermine si l'hexagone fait partie ou non du flocon
    : param O : (float) coefficient intervenant lors de la phase d'attachement, détermine si l'hexagone fait partie ou non du flocon
    : param tt : (int) temps parcouru
    : return : None

    CU : 0 <= a and 0 <= B and 0 <= O
    """
    for coord in FRONTIERE :
        voisin1 = dictionnaire[coord]['voisins']
        voisin = []
        for i in voisin1 :
            if dictionnaire[i]['appartenance'] == True :
                voisin.append(i)
        somme = 0
        if len(voisin) >= 4 :
            dictionnaire[coord]['d'] = 0
            dictionnaire[coord]['c'] = dictionnaire[coord]['c'] + dictionnaire[coord]['b']
            dictionnaire[coord]['b'] = 0
            dictionnaire[coord]['appartenance'] = True
            dictionnaire[coord]['couleur'] = (int(255 *tt/temps_max),0,int(255 *(1-tt/temps_max)))

        elif len(voisin) == 3 :
            for t in voisin1 :
                somme += dictionnaire[t]['d']
            if (somme < O and dictionnaire[coord]['b'] >= a)  or (dictionnaire[coord]['b'] >= 1) :
                dictionnaire[coord]['d'] = 0
                dictionnaire[coord]['c'] = dictionnaire[coord]['c'] + dictionnaire[coord]['b']
                dictionnaire[coord]['b'] = 0
                dictionnaire[coord]['appartenance'] = True
                dictionnaire[coord]['couleur'] = (int(255 *tt/temps_max),0,int(255 *(1-tt/temps_max)))

        elif  1 <= len(voisin) and dictionnaire[coord]['b'] > B:
            dictionnaire[coord]['d'] = 0
            dictionnaire[coord]['c'] = dictionnaire[coord]['c'] + dictionnaire[coord]['b']
            dictionnaire[coord]['b'] = 0
            dictionnaire[coord]['appartenance'] = True
            dictionnaire[coord]['couleur'] = (int(255 *tt/temps_max),0,int(255 *(1-tt/temps_max)))
    return

def bruit(coord,s) :
    """
    Définie la fonction bruit qui intègre une part de hazard faisant fluctuer la concentration de vapeur en chaque point du pavage hexagonal
    : param coord : (tuple) tuple de coordonnées de l'hexagone dans le pavage hexagonal
    : param s : (float) perturbation aléatoire
    : return : None

    CU : 0 <= s et pour un tuple de coordonnées (x,y) il faut que x et y soient positifs ou nuls et len(coord) == 2
    """
    aleatoire = randrange(-1,2,2)
    dictionnaire[coord]['d'] = (1 + aleatoire * s) * dictionnaire[coord]['d']
    return


def centre_hexagone(coord) :
    """
    Renvoie le tuple des coordonnées (en pixel) du centre d'un hexagone dont les coordonnees sont dans le pavage hexagonal
    : param coord : (tuple) coordonnées d'un hexagone dans le pavage hexagonal
    : return : (tuple) coordonnées du centre de l'hexagone en pixel

    CU : pour un tuple de coordonnées (x,y) il faut que x et y soient positifs ou nuls et len(coord) == 2

    Tests :
    >>> centre_hexagone((0,0))
    (3, 2)
    >>> centre_hexagone((100,100))
    (151, 201)
    """
    if coord[0]%2 == 0 :
        c = (int( ((3/2)*coord[0]+1)*cote ),int( (2*coord[1]+1)*demi_hauteur ))
    else :
        c = (int( ((3/2)*coord[0]+1)*cote ),int( (2*coord[1]+2)*demi_hauteur ))
    return c

def dessin_pavage(liste,tt):
    """
    Dessine un pavage hexagonal
    : param liste : (list) liste de tuples de coordonnées de tout les hexagones du pavage hexagonal
    : param tt : (int) temps parcouru

    CU : tt positif ou nul
    """
    image = Image.new("RGB", (longueur, largeur),BLACK)
    draw = ImageDraw.Draw(image)

    liste_en_pixel=[]
    for i in range(len(liste)):
        liste_en_pixel.append(centre_hexagone(liste[i]))
    for i in range(len(liste_en_pixel)):
        coordonnées_hexagone=[(liste_en_pixel[i][0] - cote//2,liste_en_pixel[i][1] - demi_hauteur),
                          (liste_en_pixel[i][0] + cote//2,liste_en_pixel[i][1] - demi_hauteur),
                          (liste_en_pixel[i][0] + cote,liste_en_pixel[i][1]),
                          (liste_en_pixel[i][0] + cote//2,liste_en_pixel[i][1] + demi_hauteur),
                          (liste_en_pixel[i][0] - cote//2,liste_en_pixel[i][1] + demi_hauteur),
                          (liste_en_pixel[i][0] - cote,liste_en_pixel[i][1])]
        draw.polygon(coordonnées_hexagone, fill = dictionnaire[liste[i]]['couleur'], outline=None)
    image.save("Flocon_test_" + str(tt) + ".png", "png")
    #image.show()

def evolution_flocon(p,k,B,O,a,u,y,s):
    """
    Fonction finale qui dessine le flocon tout les 100 tt
    : param p : (float) densité de vapeur dans chaque cellule a l'etat initial
    : param k : (float) proportion de vapeur qui se transforme en glace poru une cellule frontiere
    : param B : (float) coefficient intervenant lors de la phase d'attachement
    : param O : (float) coefficient intervenant lors de la phase d'attachement
    : param a : (float) coefficient intervenant lors de la phase d'attachement
    : param u : (float) coefficient de la phase de fonte
    : param y : (float) coefficient de la phase de fonte
    : param s : (float) perturbation aléatoire
    : return : (None)

    CU : tout les coefficients doivent etre positifs
    """

    global dictionnaire
    global CRISTAL
    global FRONTIERE

    liste = coordonnees(NOMBRE_DE_LIGNE)
    dictionnaire = coordonnees_dictionnaire(liste,p)

    for tt in range(temps_max) :

        CRISTAL = cristal(liste)
        FRONTIERE = frontiere(liste)

        for h in liste :
            bruit(h,s)
            diffusion(h)

        gel(k)
        attachement(a,B,O,tt)
        fonte(u,y)

        if tt%100 == 0 :
            dessin_pavage(liste,tt)

        tt+=1

    return

#Quelques exemples des différents paramètres attribués pour la création des images:
#Evolution6:evolution_flocon(2.0,0.8,1.7,0.25,1.9,0.0005,0.0005,0.02)
#Evolution7:evolution_flocon(1,0.4,0.65,0.7,0.7,0.4,0.25,0)
#Evolution8:evolution_flocon(1,0.4,0.65,0.7,0.7,0.4,0.25,0)
#Evolution11:evolution_flocon(1,0.4,0.65,0.7,0.7,0.4,0.25,0)
#Evolution12:evolution_flocon(1,0.4,0.65,0.7,0.4,0.4,0.25,0.05)

#Pour les évolutions ayant les même paramètres nous avons fait varier la taille de l'image ou la fréquence d'enregistrement des images.
