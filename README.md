# Simulation de la croissance d'un flocon de neige en fonction de son environnement | Python

Période de conception 1 : de 03-2017 à 04-2017
Période de conception 2 : du 20-01-2021 au 31-01-2021

Auteurs du projet :  
\- Jean-Baptiste Huber (conception 1)
\- Gaétan Simonin

Ce dépôt `Git` contient le projet `Python` sur lequel nous avons travaillé durant notre 2ème semestre d'études supérieures puis réécrit lors du 8ème semestre dans le cadre de notre formation d'ingénieur en Système Embarqué (anciennement IMA) à Polytech' Lille. N'hésitez pas à cloner ce dépôt et l'adapter à vos besoins.

## Présentation du projet

Ce projet a pour but d’implémenter une simulation vraisemblable de la génération des cristaux de neige, à partir d’une description simplifiée de la physique en oeuvre.  

La génération d’un cristal dépend d’un certain nombre de constantes, chaque choix de constantes conduisant à des résultats différents. On peut chercher les constantes permettant de reproduire fidèlement des cristaux existants, ou bien jouer avec ces constantes pour obtenir des cristaux chimériques.  

Le sujet complet regroupant une explication plus détaillée et les algorithmes utilisés est disponible à l'URL suivant : https://www.fil.univ-lille1.fr/~L1S2API/CoursTP/Projets/Flocon/sujet/source/index.html.  

## Contenu du dépôt
- `README.md` : le fichier que vous être en train de lire, présente bièvement le projet et l'utilité des fichiers qui le compose.  
- `flocon.py` : contient les fonctions principales du programme.  
- `simulationFunctions.py` : contient les fonctions de simulation et les algorithmes de calculs.  
- `drawingFunctions.py` : contient les fonctions de dessins et donc d'affichage du flocon.  
- `floconOldVersion.py` : contient le premier projet, fait en collaboration avec Jean-Baptiste Huber.  

## Avant de lancer  
** /!\ Attention, avant de lancer, vérifiez bien que vous avez les packages `progressbar2` et `Pillow`. /!\ **  

Si vous ne les possèdez pas, voici la marche à suivre pour les télécharger :  
- Télécharger `progressbar2` :  
```
pip install progressbar2
```
- Télécharger `Pillow` :  
```
pip install Pillow
```

## Lancer l'algorithme de simulation

L'algorithme est lancé par la commande :  
```
python flocon.py
```

Vous n'avez plus qu'à vous laissez guider par l'interface. Les résultats sont recuillis dans les dossiers Simulation[n], où n est le numéro de la simulation.