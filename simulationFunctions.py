#------------------------------------------------------------------
#   Write by Gaetan Simonin
#   First script from March to April 2017 (works but not perfectly)
#   Second script last week-end of Janvier 2021 (this script)
#-----------------------------------------------------------------

###############################################################
### 	Fonctions de simulation du comportement du flocon 	###
###############################################################

from random import randrange

def setInitalCoordList(n):
    l=[]
    for i in range(n+1):
        for j in range(n+1):
            l.append((j,i))
    return l

def getNeighbours(c, NOMBRE_DE_LIGNE):
    j=c[0]
    i=c[1]
    liste=[]

    if not(j%2):
    	# voisin pair
        v = [(j,i-1), (j+1,i-1), (j+1,i), (j,i+1), (j-1,i), (j-1,i-1)]
    else:
    	# voisin impair
        v = [(j,i-1), (j+1,i), (j+1,i+1), (j,i+1), (j-1,i+1), (j-1,i)]

    for t in v:
        if not(t[0]<0 or t[1]<0 or t[0]>NOMBRE_DE_LIGNE or t[1]>NOMBRE_DE_LIGNE):
        	# Suppression des voisins hors limite (hors zone)
            liste.append(t)
    return liste

def initialListToDico(coordList, constantsValuesDico):
    dico = dict()
    for t in coordList:
        dico[t] = dict()
        dico[t]['b'] = 0.0
        dico[t]['c'] = 0.0
        dico[t]['d'] = constantsValuesDico["RO"]
        dico[t]['neighbours'] = getNeighbours(t, constantsValuesDico["nbLines"])
        dico[t]['in'] = False
        dico[t]['couleur'] = (0,0,0)

    c = (constantsValuesDico["nbLines"]//2,constantsValuesDico["nbLines"]//2)
    dico[c]['b'] = 0.0
    dico[c]['c'] = 1.0
    dico[c]['d'] = 0.0
    dico[c]['in'] = True
    dico[c]['couleur'] = (0,0,255)
    return dico

def whoIsInCristal(coordList, dicoCoord):
    cristal = set()
    for i in coordList:
        if dicoCoord[i]['in'] == True:
            cristal.add(i)
    return cristal

def whoIsInBorder(cristalList, dicoCoord):
    border = set()
    for c in cristalList:
        for i in dicoCoord[c]['neighbours']:
        	# la frontière est formée des coord voisines du cristal
            if dicoCoord[i]['in'] == False:
                border.add(i)
    return border

def diffusion(coord, cristal, dicoCoord):
	if not(coord in cristal):
		somme = 0.0
		voisin = dicoCoord[coord]['neighbours']
		for c in voisin:
			if c in cristal:
				somme += dicoCoord[coord]['d']
			else:
				somme += dicoCoord[c]['d']
		dicoCoord[coord]['d'] = somme/7
	return

def gel(constantsValuesDico, border, dicoCoord):
    for c in border:
        dicoCoord[c]['b'] += (1 - constantsValuesDico["KAPPA"]) * dicoCoord[c]['d']
        dicoCoord[c]['c'] += constantsValuesDico["KAPPA"] * dicoCoord[c]['d']
        dicoCoord[c]['d'] = 0
    return

def fonte(constantsValuesDico, border, dicoCoord):
	for c in border:
		dicoCoord[c]['d'] += constantsValuesDico["MU"] * dicoCoord[c]['b'] + constantsValuesDico["GAMMA"] * dicoCoord[c]['c']
		dicoCoord[c]['b'] *= 1-constantsValuesDico["MU"]
		dicoCoord[c]['c'] *= 1-constantsValuesDico["GAMMA"]
	return

def addToCristal(c, tt, temps_max, border, cristal, dicoCoord):
	dicoCoord[c]['c'] += dicoCoord[c]['b']
	dicoCoord[c]['b'] = dicoCoord[c]['d'] = 0
	dicoCoord[c]['in'] = True
	dicoCoord[c]['couleur'] = (int(255 *tt/temps_max),0,int(255 *(1-tt/temps_max)))
	cristal.add(c)
	border.discard(c)
	return

def attachement(constantsValuesDico, tt, border, cristal, dicoCoord):
    nb=0
    for c in border:
        for v in dicoCoord[c]['neighbours']:
            if (v in cristal):
                nb += 1

        if (nb>3) or ((nb == 1 or nb == 2) and dicoCoord[c]['b']>constantsValuesDico["BETA"]) or ((nb == 3) and (dicoCoord[c]['b']>1)):
            addToCristal(c, tt, constantsValuesDico["time"], border, cristal, dicoCoord)
            return

        if nb==3:
            somme = 0.0
            for v in dicoCoord[c]['neighbours']:
                somme += dicoCoord[v]['d']
            if (somme<constantsValuesDico["THETA"]) and not(dicoCoord[c]['b']<constantsValuesDico["ALPHA"]):
                addToCristal(c, tt, constantsValuesDico["time"], border, cristal, dicoCoord)
                return
    return

def bruit(c, constantsValuesDico, dicoCoord):
    dicoCoord[c]['d'] *= (1 + randrange(-1,2,2) * constantsValuesDico["SIGMA"])
    return
