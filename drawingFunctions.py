#------------------------------------------------------------------
#	Write by Gaetan Simonin
#	First script from March to April 2017 (works but not perfectly)
#	Second script last week-end of Janvier 2021 (this script)
#-----------------------------------------------------------------

###############################################################
### 			Fonctions de tracage des images				###
###############################################################

from PIL import Image, ImageDraw

def getHexagoneCentralPixel(coordList, constantsValuesDico):
	side = constantsValuesDico["side"]
	half_height = constantsValuesDico["half_height"]
	centralPixelsDico = dict()
	for c in coordList:
		if not(c[0]%2):
			centralPixelsDico[c] = (int((3*c[0]/2 + 1)*side), int((2*c[1]+1)*half_height))
		else:
			centralPixelsDico[c] = (int((3*c[0]/2 + 1)*side), int((2*c[1]+2)*half_height))
	return centralPixelsDico

def getHexagonePixelTops(coordList, centralPixelsDico, constantsValuesDico):
	side = constantsValuesDico["side"]
	half_height = constantsValuesDico["half_height"]
	pixelsCoordHexagoneTopsDico = dict()
	for c in coordList:
		cp = centralPixelsDico[c]
		pixelsCoordHexagoneTopsDico[c]=[	(cp[0] - side//2,cp[1] - half_height),
		                          			(cp[0] + side//2,cp[1] - half_height),
		                          			(cp[0] + side,cp[1]),
				                        	(cp[0] + side//2,cp[1] + half_height),
				                        	(cp[0] - side//2,cp[1] + half_height),
				                        	(cp[0] - side,cp[1])	]
	return pixelsCoordHexagoneTopsDico

def drawImage(coordList, pixelsCoordHexagoneTopsDico, tt, size, dicoCoord):
    black = (0, 0, 0)
    image = Image.new("RGB", size, black)
    draw = ImageDraw.Draw(image)

    for c in coordList:
        draw.polygon(pixelsCoordHexagoneTopsDico[c], fill = dicoCoord[c]['couleur'], outline=None)

    image.save("Evolution_" + str(tt) + ".png", "png")
    return
