#------------------------------------------------------------------
#	Write by Gaetan Simonin
#	First script from March to April 2017 (works but not perfectly)
#	Second script last week-end of Janvier 2021 (this script)
#-----------------------------------------------------------------

from drawingFunctions import *
from simulationFunctions import *
from math import sqrt
import os, progressbar

def main():
	print("\n\t________________________________________________\n")
	print("\t| Welcome to the Snowflake Expansion Simulator |")
	print("\t________________________________________________\n")

	NOMBRE_DE_LIGNE = 100
	again = 'y'
	nbSimul = 0
	currentPath = os.getcwd()

	while(again == 'y' or again == 'Y'):
		nbSimul += 1
		choise = 0
		while(choise != 1 and choise != 2 and choise != 3):
			print("Please choose what you want to do :")
			print("1 - Provide your values")
			print("2 - Use a preselection of values")
			choise = input()
			try:
				choise = int(choise)
			except ValueError:
				print("\nPlease enter a number\n")

		if (choise == 3):
			print("Bye\n")
			return

		if (choise == 1):
			constantsValuesList = getConstants()
		else:
			preselection = [	(0.65, 1.5, 0.015, 0.00001, 1.75, 0.26, 0.2, 0.0, 101),
								(2.0, 0.8, 0.0005, 0.0005, 1.7, 0.25, 1.9, 0.02, 2501),
								(1, 0.4, 0.4, 0.25, 0.65, 0.7, 0.7, 0 , 2501),
								(1, 0.4, 0.4, 0.25, 0.35, 0.7, 0.12, 0, 2501),
								(1, 0.2, 0.6, 0.25, 0.65, 0.7, 0.7, 0, 2501),
								(1, 0.4, 0.4, 0.25, 0.65, 0.7, 0.4, 0.05, 2501)	]
			constantsValuesList = preselection[0]#preselection[randrange(5)]

		constantsValuesDico = setConstants(constantsValuesList, NOMBRE_DE_LIGNE)

		dirPath = currentPath + "/Simulation" + str(nbSimul)
		try:
			os.mkdir(dirPath)
		except FileExistsError:
			os.chdir(dirPath)
		except FileNotFoundError:
			print("Wrong directory path\n")
			return
		else:
			os.chdir(dirPath)
		finally:
			simulationLauncher(constantsValuesDico)

		os.chdir(currentPath)
		again = 'x'
		while(again != 'n' and again != 'y' and again != 'N' and again != 'Y'):
			print("\nThe simulation is finished\n")
			print("Do you want to do it again ?")
			print("(y/Y) - Yes")
			print("(n/N) - No")
			again = input()

	print("Bye\n")
	return

def getConstants():
	charConstants = ["RO", "KAPPA", "MU", "GAMMA", "BETA", "THETA", "ALPHA", "SIGMA"]
	constantsValuesList = list()
	print("Please provide the values you want to use :")
	for c in charConstants:
		const = -1.0
		while(const<0.0 or const>4):
			const = input(c + " : ")
			try :
				const = float(const)
			except ValueError:
				print("Entrez un nombre réel entre 0 et 2")
				const = -1.0
		constantsValuesList.append(const)
	const = -1
	while(const <= 0):
		const = input("simulation time : ") 
		try:
			const = int(const)
		except ValueError:
			print("Entrez un nombre entier positif")
			const =  -1
	constantsValuesList.append(const)
	return constantsValuesList

def simulationLauncher(constantsValuesDico):
	widgets=['Loading: ', progressbar.AnimatedMarker(), progressbar.Bar('*'), ' [Counter: ', 
				progressbar.Counter(), ' | ', progressbar.Timer(), '] ']
	bar = progressbar.ProgressBar(max_value=constantsValuesDico["time"], widgets=widgets).start()

	coordList = setInitalCoordList(constantsValuesDico["nbLines"])
	dicoCoord = initialListToDico(coordList, constantsValuesDico)
	centralPixelsDico = getHexagoneCentralPixel(coordList, constantsValuesDico)
	pixelsCoordHexagoneTopsDico =  getHexagonePixelTops(coordList, centralPixelsDico, constantsValuesDico)

	for tt in range(constantsValuesDico['time']):
		bar.update(tt)

		cristal = whoIsInCristal(coordList, dicoCoord)
		border = whoIsInBorder(cristal, dicoCoord)
		
		for c in coordList:
			bruit(c, constantsValuesDico, dicoCoord)
			diffusion(c, cristal, dicoCoord)
		
		gel(constantsValuesDico, border, dicoCoord)
		attachement(constantsValuesDico, tt, border, cristal, dicoCoord)
		fonte(constantsValuesDico, border, dicoCoord)

		if not(tt%10):
			drawImage(coordList, pixelsCoordHexagoneTopsDico, tt, (constantsValuesDico["lengh"], constantsValuesDico["width"]), dicoCoord)
	return

def setConstants(constantsValuesList, nbLines):
	constants = {"RO": 0.0, "KAPPA": 0.0, "GAMMA": 0.0, "BETA": 0.0, "THETA": 0.0, "ALPHA": 0.0, "SIGMA": 0.0, "time": 0}
	constants["RO"] = float(constantsValuesList[0])
	constants["KAPPA"] = float(constantsValuesList[1])
	constants["MU"] = float(constantsValuesList[2])
	constants["GAMMA"] = float(constantsValuesList[3])
	constants["BETA"] = float(constantsValuesList[4])
	constants["THETA"] = float(constantsValuesList[5])
	constants["ALPHA"] = float(constantsValuesList[6])
	constants["SIGMA"] = float(constantsValuesList[7])
	constants["time"] = int(constantsValuesList[8])+1
	constants["nbLines"] = nbLines
	constants["side"] = 1
	constants["half_height"] = sqrt(constants["side"]**2-(constants["side"]//2)**2)
	constants["lengh"] = int(((3/2*nbLines)+2)*constants["side"])
	constants["width"] = int((nbLines*2+1)*constants["half_height"])
	return constants

if __name__ == "__main__":
    main()